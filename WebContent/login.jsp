<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
		<link href="./style.css" rel="stylesheet" type="text/css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>ログイン画面</title>
    </head>
    <body>
        <div class="main-contents">
            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                </div>
                <c:remove var="errorMessages" scope="session"/>
            </c:if>
            <form action="login" method="post"><br />
                <label for="loginId">ログインID</label>
         		<c:if test="${ not empty loginId }">
                	<input name="loginId" id="loginId" value="<%=request.getAttribute("loginId") %>"/> <br />
       			</c:if>
         		<c:if test="${ empty loginId }">
                	<input name="loginId" id="loginId" value=""/> <br />
       			</c:if>

                <label for="password">パスワード</label>
                <input name="password" type="password" id="password"/> <br />

                <input type="submit" value="ログイン" /> <br />
            </form>
			<br />
            <div class="copyright"> Copyright(c) FukudaNaoki</div>
        </div>
    </body>
</html>