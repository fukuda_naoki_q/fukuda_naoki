<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<link href="./style.css" rel="stylesheet" type="text/css">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>ユーザー新規登録</title>
	</head>
	<body>
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>
		<div class="head-menu">
			<a href="userManager">戻る</a>
		</div>
		<br />
		<div class="main-contents">
			<form action="signup" method="post">
				<label for="login_id">ログインID：</label><br />
				<input name="login_id" value="${login_id}"id="login_id" /><br />
				<br />
				<label for="password">パスワード：</label><br />
				<input name="password" value="${password}" type="password" id="password" /><br />
				<br />
				<label for="confirm">パスワード(確認用)：</label><br />
				<input name="confirm" value="${password}" type="password" id="confirm" /><br />
				<br />
				<label for="name">名前：</label><br />
				<input name="name" value="${name}" id="name" /><br />
				<br />
				<label for="branch_id">所属支店：</label><br />
                <select name = "branch_id" id="branch_id">
	                <c:forEach items="${branches}" var="branch">
	                	<c:if test="${branch.id == selectedBranchId}">
	                		<option selected value="${branch.id}">${branch.name}</option>
	                	</c:if>
	                	<c:if test="${branch.id != selectedBranchId}">
	                		<option value="${branch.id}">${branch.name}</option>
	                	</c:if>
	                </c:forEach>
                </select><br />
				<br />
				<label for="position_id">所属部署・役職：</label><br />
                <select name="position_id"id="position_id">
	                <c:forEach items="${positions}" var="position">
	                	<c:if test="${position.id == selectedPositionId}">
		              	  <option selected value="${position.id}">${position.name}</option>
	                	</c:if>
	                	<c:if test="${position.id != selectedPositionId}">
		              	  <option value="${position.id}">${position.name}</option>
	                	</c:if>
	                </c:forEach>
                </select><br />
				<br />
				<input type="submit" value="登録" />
				<br />
				<br />
			</form>
			<div class="copyright">Copyright(c)Fukuda Naoki</div>
		</div>
	</body>
</html>