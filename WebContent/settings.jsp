<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
		<link href="./style.css" rel="stylesheet" type="text/css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>${editUser.name}の設定</title>
        <link href="css/style.css" rel="stylesheet" type="text/css">
    </head>
    <body>
		<c:if test="${ not empty errorMessages }">
		    <div class="errorMessages">
		        <ul>
		            <c:forEach items="${errorMessages}" var="message">
		                <li><c:out value="${message}" />
		            </c:forEach>
		        </ul>
		    </div>
		    <c:remove var="errorMessages" scope="session"/>
		</c:if>
		<div class="head-menu">
			<a href="userManager">戻る</a>
		</div>
        <div class="main-contents">
            <form action="settings" method="post"><br />
                <input name="id" value="${editUser.id}" id="id" type="hidden"/>
				ログインID：<br />
                <input name="loginId" value="${editUser.loginId}" id="loginId"/><br />
                <br />
				 パスワード：<br />
                <input name="password" type="password" value="" id="password"/><br />
				<br />
				パスワード(確認用)：<br />
                <input name="confirm" type="password" value="" id="confirm"/><br />
				<br />
				名称：<br />
                <input name="name" value="${editUser.name}" id="name" />
				<!-- 他人のユーザー情報編集の場合 -->
               	<c:if test="${editedLoginId != loginUserId}">
               		<br />
					<br />
					<label for="branch_id">所属支店：</label><br />
		                <select name = "branch_id" id="branch_id">
			                <c:forEach items="${branches}" var="branch">
			                	<c:if test="${branch.id == selectedBranchId}">
			                		<option selected value="${branch.id}">${branch.name}</option>
			                	</c:if>
			                	<c:if test="${branch.id != selectedBranchId}">
			                		<option value="${branch.id}">${branch.name}</option>
			                	</c:if>
			                </c:forEach>
		                </select>
					<br />
					<br />
					<label for="position_id">所属部署・役職：</label><br />
	                <select name="position_id"id="position_id">
		                <c:forEach items="${positions}" var="position">
		                	<c:if test="${position.id == selectedPositionId}">
			              	  <option selected value="${position.id}">${position.name}</option>
		                	</c:if>
		                	<c:if test="${position.id != selectedPositionId}">
			              	  <option value="${position.id}">${position.name}</option>
		                	</c:if>
		                </c:forEach>
	                </select>
				</c:if>
				<!-- 自分のユーザー情報編集の場合 -->
               	<c:if test="${editedLoginId == loginUserId}">
	                <input type="hidden" name = "branch_id" id = "branch_id" value = "${editUser.getBranchId()}">
	                <input type="hidden" name = "position_id" id  ="position_id" value = "${editUser.getPositionId()}">
				</c:if>
				<br />
				<br />
                <input type="submit" value="適用" onclick="javascript:undisabled();"/>
                <br />
                <br />
            </form>
            <div class="copyright">Copyright(c)Fukuda Naoki</div>
        </div>
    </body>
</html>