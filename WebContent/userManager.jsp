<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC>
<html>
<head>
	<link href="./style.css" rel="stylesheet" type="text/css">
	<script type="text/javascript">
		function check(){
			if(window.confirm('ユーザーの有効/無効を切り替えますか？')){
				return true; // 「OK」時は送信を実行
			} else {
				return false; // 送信を中止
			}
		}
	</script>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>ユーザー管理画面</title>
</head>
<body>
	<c:if test="${ not empty errorMessages }">
		<div class="errorMessages">
			<ul>
				<c:forEach items="${errorMessages}" var="message">
					<li><c:out value="${message}" />
				</c:forEach>
			</ul>
		</div>
		<c:remove var="errorMessages" scope="session" />
	</c:if>
	<div class="head-menu">
		<a href="./">戻る</a>
		<a href="signup">ユーザー登録</a>
	</div>
	<br />
	<div class="users">
		<table border="1">
			<tr>
				<th>ログインID</th><th>名称</th><th>支店</th><th>部署・役職</th><th>稼働状況</th><th>管理</th>
			</tr>
			<c:forEach items="${users}" var="user">
				<tr>
					<td>
						<c:out value="${user.loginId}" />
					</td>
					<td>
						<c:out value="${user.name}" />
					</td>
					<td>
						<c:out value="${user.branchName}" />
					</td>
					<td>
						<c:out value="${user.positionName}" />
					</td>
					<td>
						<form action="switch" method="post" onSubmit="return check()">
							<c:if test="${user.id != loginUser}">
								<c:if test="${user.isStopped == true}">
									<input type="hidden" name="user_id" value="${user.id}">
									<input type="submit" value="停止中">
								</c:if>
								<c:if test="${user.isStopped == false}">
									<input type="hidden" name="user_id" value="${user.id}">
									<input type="submit" value="稼動中">
								</c:if>
							</c:if>
						</form>
					</td>
					<td>
						<a href="settings?user_id=${user.id}">編集</a>
					</td>
				</tr>
			</c:forEach>
		</table>
		<br />
	</div>
	<div class="copyright">
		Copyright(c) FukudaNaoki
	</div>
</body>
</html>