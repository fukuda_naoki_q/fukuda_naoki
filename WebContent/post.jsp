<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<link href="./style.css" rel="stylesheet" type="text/css">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>新規投稿画面</title>
</head>
<body>
	<c:if test="${ not empty errorMessages }">
		<div class="errorMessages">
			<ul>
				<c:forEach items="${errorMessages}" var="message">
					<li><c:out value="${message}" />
				</c:forEach>
			</ul>
		</div>
		<c:remove var="errorMessages" scope="session"/>
	</c:if>
	<div class="head-menu">
		<a href="./">戻る</a>
	</div>
	<div class="main-contents">
		<c:if test="${ not empty loginUser }">
			<div class="form-area">
				<form action="post" method="post">
					件名<br />
					<input name="title" class="title" value="${title}"/><br />
					本文<br />
					<textarea name="text" cols="100" rows="10" class="text"><c:if test="${ not empty text}"><%=request.getParameter("text") %></c:if></textarea><br />
					カテゴリー<br />
					<input name="category" class="category" value="${category}"/><br />
					<br />
					<input type="submit" value="投稿"><br />(1000文字まで)
				</form>
			</div>
		</c:if>
	</div>
	<br />
	<div class="copyright">
		Copyright(c) FukudaNaoki
	</div>
</body>
</html>