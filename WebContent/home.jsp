<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
	<head>
	    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	    <link href="./style.css" rel="stylesheet" type="text/css" />
	    <script type="text/javascript">
	    function commentDeleteCheck(){
	        if(window.confirm('コメントを削除しますか？')){
	            return true; // 「OK」時は送信を実行
	        } else {
	            return false; // 送信を中止
	        }
	    }
	    function postDeleteCheck(){
	        if(window.confirm('投稿を削除しますか？')){
	            return true; // 「OK」時は送信を実行
	        } else {
	            return false; // 送信を中止
	        }
	    }
	    </script>
	    <title>ホーム画面</title>
	</head>
	<body>
	    <c:if test="${ not empty errorComments }">
	        <div class="errorMessages">
	                <c:forEach items="${errorComments}" var="errorComments">
	                    <c:out value="${errorComments}" />
	                </c:forEach>
	        </div>
	        <c:remove var="errorComments" scope="session"/>
	    </c:if>
	    <div class="header">
	    	${loginUser.name}さん
	    </div>
	    <div class="left-column">
	        <div class="menu">
	            <a href="post">新規投稿</a><br />
	            <c:if test="${ empty isManager }">
	                <a href="userManager">ユーザー管理</a><br />
	            </c:if>
	            ---<br />
	            <a href="login">ログアウト</a>
	        </div>
		    <div class="search-box">
		        <!-- 絞込検索ボックスブロック -->
		        [検索]
		        <form name="searchButton" action="index.jsp" method="get">
		            <!-- 日付検索 -->
		            期間指定：<br />
		            <input type="date" name="searchByDateStart" value="${searchByDateStart}"></input><br />
		            　　　～<br />
		            <input type="date" name="searchByDateEnd" value="${searchByDateEnd}"></input><br />
		            <!-- カテゴリ検索 -->
		            <br />
		            カテゴリ指定：<br />
		             <input type="text" name="searchByCategory" value="${searchByCategory}"></input><br />
		             <br />
		             <!-- 検索ボタン -->
		             <input type="submit" value="検索">
	             </form>
	         </div>
        </div>
        <div class="main-column">
         <!-- 投稿ブロック -->
	            <div class="posts">
	                <c:forEach items="${posts}" var="post">
	                    <div class="post">
	                        <b>件名：</b>
	                        <span class="title"><c:out value="${post.title}" /></span><br />
	                        <b>本文：<br /></b>
	                        <span class="text"><c:forEach var="str" items="${fn:split(post.text,'
')}" ><c:out value="${str}" /><br /></c:forEach></span><br />
	                        <b>カテゴリー：</b>
	                        <span class="category"><c:out value="${post.category}" /></span><br />
	                       <b>投稿者：</b>
	                        <span class="name"><c:out value="${post.name}" /></span><br />
	                        <b>投稿日時：</b>
	                        <fmt:formatDate value="${post.createdAt}" pattern="yyyy.MM.dd HH:mm:ss" /><br />
	                        <c:if test="${post.userId == loginUser.id}">
	                            <form name="deletePost" action="deletePost" method="post" onSubmit="return postDeleteCheck()">
	                                <input type="hidden" name="postId" value="${post.id}">
	                                <input type="submit" value="投稿削除">
	                            </form>
	                        </c:if>
	                   </div>
	                        <!-- コメントブロック -->
	                        <c:forEach items="${comments}" var="comment">
	                            <c:if test="${post.id == comment.postId }">
	                   <div class="comments">
	                                <b>コメント：<br /></b>
	                                    <span class="comment">
	                                    <c:forEach var="str" items="${fn:split(comment.text,'
')}" ><c:out value="${str}" /><br /></c:forEach></span><br />
	                                <b>登録者：</b>
	                                    <span class="commentUsername"><c:out value="${comment.username}" /></span><br />
	                                <b>登録日時：</b>
	                                    <fmt:formatDate value="${comment.createdAt}" pattern="yyyy/MM/dd HH:mm:ss" /><br />
	                                <c:if test="${comment.userId == loginUser.id}">
	                                    <form name="deleteComment" action="deleteComment" method="post"  onSubmit="return commentDeleteCheck()">
	                                        <input type="hidden" name="commentId" value="${comment.id}">
	                                        <input type="submit" value="コメント削除">
	                                    </form>
	                                </c:if>
	                   </div>
	                            </c:if>
	                        </c:forEach>
	                   <div class="comment-box">
	                        <!-- コメント入力ボックスブロック -->
	                        <form action="comment" method="post">
	                            <b>コメント入力：<br /></b>
	                            <input type="hidden" name="postId" value="${post.id}">
	                            <textarea name="comment" cols="100" rows="4" class="comment"></textarea><br />
	                            <input type="submit" value="コメント">(500文字まで)
	                        </form>
	                    </div>
	                </c:forEach>
	            </div>
	        <br />
	        <div class="copyright"> Copyright(c) Fukuda Naoki</div>
	    </div>
	</body>
</html>