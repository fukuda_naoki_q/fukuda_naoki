package board.service;

import static board.utils.CloseableUtil.*;
import static board.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import board.beans.Post;
import board.dao.PostDao;

public class PostService {

	private static final int LIMIT_NUM = 1000;

    public void register(Post post) {

        Connection connection = null;
        try {
            connection = getConnection();

            PostDao postDao = new PostDao();
            postDao.insert(connection, post);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

	public List<Post> getPost(String startDate, String endDate, String category) {
	    Connection connection = null;
	    try {
	        connection = getConnection();

	        PostDao postDao = new PostDao();
	        List<Post> ret = postDao.getUserPosts(connection, LIMIT_NUM, startDate, endDate, category);

	        commit(connection);

	        return ret;
	    } catch (RuntimeException e) {
	        rollback(connection);
	        throw e;
	    } catch (Error e) {
	        rollback(connection);
	        throw e;
	    } finally {
	        close(connection);
	    }
	}
    public void deletePost(Post post) {

        Connection connection = null;
        try {
            connection = getConnection();

            PostDao postDao = new PostDao();
            postDao.delete(connection, post);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

}