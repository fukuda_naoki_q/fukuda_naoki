package board.service;

import static board.utils.CloseableUtil.*;
import static board.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import board.beans.Branch;
import board.dao.BranchDao;

public class BranchService {
	private static final int LIMIT_NUM = 1000;

	public Branch getBranch(int branchId) {
	    Connection connection = null;
	    try {
	        connection = getConnection();

	        BranchDao branchDao = new BranchDao();
	        Branch branch = branchDao.getBranch(connection, branchId);

	        commit(connection);

	        return branch;
	    } catch (RuntimeException e) {
	        rollback(connection);
	        throw e;
	    } catch (Error e) {
	        rollback(connection);
	        throw e;
	    } finally {
	        close(connection);
	    }
	}

	public List<Branch> getBranch() {
	    Connection connection = null;
	    try {
	        connection = getConnection();

	        BranchDao branchDao = new BranchDao();
	        List<Branch> branches = branchDao.getBranches(connection, LIMIT_NUM);

	        commit(connection);

	        return branches;
	    } catch (RuntimeException e) {
	        rollback(connection);
	        throw e;
	    } catch (Error e) {
	        rollback(connection);
	        throw e;
	    } finally {
	        close(connection);
	    }
	}
}