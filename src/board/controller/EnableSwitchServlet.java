package board.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import board.beans.User;
import board.service.UserService;

@WebServlet(urlPatterns = { "/switch" })
public class EnableSwitchServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {
		List<User> users = new UserService().getUsersWithBranchesNameAndPositionsName();
		for ( int i = 0; i < users.size(); i++) {
			if ( users.get(i).getId() == Integer.valueOf(request.getParameter("user_id"))) {
				if ( users.get(i).getIsStopped() == true ) {
					users.get(i).setIsStopped(false);
				} else {
					users.get(i).setIsStopped(true);
				}
				new UserService().update(users.get(i), false);
    		}
		}
		response.sendRedirect("userManager");
    }
}
