package board.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import board.beans.User;
import board.service.UserService;

@WebServlet(urlPatterns = { "/userManager" })
public class UserManagerServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {
		HttpSession session = request.getSession();
        if ( ((User) session.getAttribute("loginUser")).getBranchId() != 1 ||
       		 ((User) session.getAttribute("loginUser")).getPositionId() != 1) {
        	response.sendRedirect("./");
        	return;
        }
		List<User> users = new UserService().getUsersWithBranchesNameAndPositionsName();
		request.setAttribute("users", users);
        request.setAttribute("loginUser", ((User)session.getAttribute("loginUser")).getId());
        request.getRequestDispatcher("/userManager.jsp").forward(request, response);
    }
}
