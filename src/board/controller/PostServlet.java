package board.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import board.beans.Post;
import board.beans.User;
import board.service.PostService;
@WebServlet(urlPatterns = { "/post" })
public class PostServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {
        request.getRequestDispatcher("post.jsp").forward(request, response);
    }
    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {
        HttpSession session = request.getSession();
        List<String> posts = new ArrayList<String>();
        if (isValid(request, posts) == true) {
            User user = (User) session.getAttribute("loginUser");
            Post post = new Post();
            post.setTitle(request.getParameter("title"));
            post.setText(request.getParameter("text"));
            post.setCategory(request.getParameter("category"));
            post.setUserId(String.valueOf(user.getId()));
            new PostService().register(post);
            response.sendRedirect("./");
        } else {
            session.setAttribute("errorMessages", posts);
            request.setAttribute("title", request.getParameter("title"));
            request.setAttribute("text", request.getParameter("text"));
            request.setAttribute("category", request.getParameter("category"));
            request.getRequestDispatcher("post.jsp").forward(request, response);
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> posts) {
    	String title = request.getParameter("title");
        String text = request.getParameter("text");
        String category = request.getParameter("category");
        if (StringUtils.isBlank(title) == true) {
            posts.add("件名を入力してください");
        }
        if (StringUtils.isBlank(text) == true) {
            posts.add("本文を入力してください");
        }
        if (StringUtils.isBlank(category) == true) {
            posts.add("カテゴリーを入力してください");
        }
        if (30 < title.length()) {
        	posts.add("件名を30文字以下で入力してください");
        }
        if (1000 < text.length()) {
        	posts.add("本文を1000文字以下で入力してください");
        }
        if (10 < category.length()) {
        	posts.add("カテゴリーを10文字以下で入力してください");
        }
        if (posts.size() == 0) {
            return true;
        } else {
            return false;
        }
    }
}