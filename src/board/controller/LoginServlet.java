package board.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import board.beans.User;
import board.service.LoginService;

@WebServlet(urlPatterns = { "/login"})
public class LoginServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {
        HttpSession session = request.getSession();
        session.setAttribute("errorMessages", session.getAttribute("errorMessages"));
        request.getRequestDispatcher("login.jsp").forward(request, response);
    }
    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {
        String loginId = request.getParameter("loginId");
        String password = request.getParameter("password");
        HttpSession session = request.getSession();
        String idPattern = "[0-9a-zA-Z]{6,20}";
        String pwPattern = "^[ -~]{6,20}$";
        if (!loginId.matches(idPattern) ||
        	!password.matches(pwPattern) ) {
            List<String> messages = new ArrayList<String>();
            messages.add("ログインに失敗しました。");
            request.setAttribute("loginId",loginId );
            session.setAttribute("errorMessages", messages);
            request.getRequestDispatcher("login.jsp").forward(request, response);
            return;
        }
        LoginService loginService = new LoginService();
        User user = loginService.login(loginId, password);
        if (user != null &&
        	!user.getIsStopped()) {
        	session.removeAttribute("loginUser");
            session.setAttribute("loginUser", user);
            List<String> messages = new ArrayList<String>();
            session.setAttribute("errorMessages", messages);
            response.sendRedirect("./");
        } else {
            List<String> messages = new ArrayList<String>();
            messages.add("ログインに失敗しました。");
            request.setAttribute("loginId",loginId );
            session.setAttribute("errorMessages", messages);
            request.getRequestDispatcher("login.jsp").forward(request, response);
        }
    }
}