package board.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import board.beans.Comment;
import board.beans.User;
import board.service.CommentService;


@WebServlet(urlPatterns = { "/comment" })
public class CommentServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {
        HttpSession session = request.getSession();
        List<String> comments = new ArrayList<String>();
        if (isValid(request, comments) == true) {
            User user = (User) session.getAttribute("loginUser");
            Comment comment = new Comment();
            comment.setPostId(request.getParameter("postId"));
            comment.setText(request.getParameter("comment"));
            comment.setUserId(String.valueOf(user.getId()));
            new CommentService().register(comment);
            response.sendRedirect("./");
        } else {
            session.setAttribute("errorComments", comments);
            response.sendRedirect("./");
        }
    }
    private boolean isValid(HttpServletRequest request, List<String> comment) {
    	String strComment = request.getParameter("comment");
        if (StringUtils.isBlank(strComment) == true) {
        	comment.add("コメントを入力してください");
        	return false;
        }
        if (strComment.length() > 500) {
        	comment.add("コメントは500文字まで入力可能です");
        	return false;
        }
        return true;
    }
}