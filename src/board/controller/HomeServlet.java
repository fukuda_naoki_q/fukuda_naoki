package board.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import board.beans.Comment;
import board.beans.Post;
import board.beans.User;
import board.service.CommentService;
import board.service.PostService;
@WebServlet(urlPatterns = { "/index.jsp" })
public class HomeServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    @Override
    protected void doGet(HttpServletRequest request,
        HttpServletResponse response) throws IOException, ServletException {
		String searchByDateStart = request.getParameter("searchByDateStart");
		String searchByDateEnd = request.getParameter("searchByDateEnd");
		String searchByCategory = null;
		if ( StringUtils.isBlank(searchByDateStart) ) {
			searchByDateStart = "";
		}
		if ( StringUtils.isBlank(searchByDateEnd) ) {
			searchByDateEnd = "";
		}
		try {
			searchByCategory = new String (request.getParameter("searchByCategory").getBytes("ISO-8859-1"));
		} catch(NullPointerException e) {
			searchByCategory = "";
		}
		request.setAttribute("searchByDateStart", searchByDateStart);
		request.setAttribute("searchByDateEnd", searchByDateEnd);
		request.setAttribute("searchByCategory", searchByCategory);
		HttpSession session = request.getSession();
		List<Post> posts = null;
		List<Comment> comments = new CommentService().getComment();
		User user = (User) session.getAttribute("loginUser");
		if ( searchByCategory.length() > 10 ) {
		    session.setAttribute("errorComments", "カテゴリ名は10文字以下で検索してください");
		    posts = new PostService().getPost("", "", "");
		} else {
			posts = new PostService().getPost(searchByDateStart, searchByDateEnd, searchByCategory);
		}
		request.setAttribute("posts", posts);
		request.setAttribute("comments", comments);
		request.setAttribute("loginUser", user);
		if ( user.getBranchId() != 1 ||
			 user.getPositionId() != 1 ) {
			request.setAttribute("isManager", "false");
		}
		request.getRequestDispatcher("home.jsp").forward(request, response);
    }
}
