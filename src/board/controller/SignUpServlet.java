package board.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import board.beans.Branch;
import board.beans.Position;
import board.beans.User;
import board.service.BranchService;
import board.service.PositionService;
import board.service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {
		HttpSession session = request.getSession();
        if ( ((User) session.getAttribute("loginUser")).getBranchId() != 1 ||
       		 ((User) session.getAttribute("loginUser")).getPositionId() != 1) {
        	response.sendRedirect("./");
        	return;
        }
		List<Branch> branches = new BranchService().getBranch();
        List<Position> positions = new PositionService().getPosition();
        request.setAttribute("branches", branches);
        request.setAttribute("positions", positions);
        request.getRequestDispatcher("signup.jsp").forward(request, response);
    }
    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {
        List<String> messages = new ArrayList<String>();
        HttpSession session = request.getSession();
        User user = getInputUser(request);
        if (isValid(request, messages) == true) {
            new UserService().register(user);
            response.sendRedirect("userManager");
        } else {
            session.setAttribute("errorMessages", messages);
    		List<Branch> branches = new BranchService().getBranch();
            List<Position> positions = new PositionService().getPosition();
            request.setAttribute("login_id", user.getLoginId());
            request.setAttribute("name", user.getName());
            request.setAttribute("branches", branches);
            request.setAttribute("positions", positions);
            request.setAttribute("selectedBranchId", Integer.valueOf(request.getParameter("branch_id")));
            request.setAttribute("selectedPositionId", Integer.valueOf(request.getParameter("position_id")));
            request.getRequestDispatcher("signup.jsp").forward(request, response);
        }
    }
    private User getInputUser(HttpServletRequest request)
            throws IOException, ServletException {
        User inputUser = new User();
        inputUser.setLoginId(request.getParameter("login_id"));
        inputUser.setPassword(request.getParameter("password"));
        inputUser.setName(request.getParameter("name"));
        inputUser.setBranchId(Integer.valueOf(request.getParameter("branch_id")));
        inputUser.setPositionId(Integer.valueOf(request.getParameter("position_id")));
        inputUser.setIsStopped(false);
        return inputUser;
    }
    private boolean isValid(HttpServletRequest request, List<String> messages) {
        String loginId = request.getParameter("login_id");
        String password = request.getParameter("password");
        String confirm = request.getParameter("confirm");
        String name = request.getParameter("name");
        String branchId = request.getParameter("branch_id");
        String positionId = request.getParameter("position_id");
        String idPattern = "[0-9a-zA-Z]{6,20}";
        String pwPattern = "^[ -~]{6,20}$";
        if (StringUtils.isBlank(loginId)) {
            messages.add("ログインIDを入力してください");
        }
        if (!loginId.matches(idPattern)) {
            messages.add("ログインIDは半角英数字の6文字以上20文字以下で入力してください");
        }
        //loginIdの重複チェック
        UserService userService = new UserService();
        User user = userService.getUserByLoginId(loginId);
        if ( user != null ) {
            messages.add("入力されたログインIDは既に使用されています");
        }
        if (!StringUtils.isBlank(password) &&
            	StringUtils.isBlank(confirm)) {
                messages.add("パスワード(確認用)を入力してください");
        }
        if (StringUtils.isBlank(password) &&
         	!StringUtils.isBlank(confirm)) {
            messages.add("パスワードを入力してください");
        }
        if (!password.matches(pwPattern)) {
            messages.add("パスワードは記号を含む全ての半角文字6文字以上20文字以下で入力してください");
        }
        if ( !StringUtils.isBlank(password) &&
                !StringUtils.isBlank(confirm) &&
                !password.matches(confirm) ) {
            messages.add("「パスワード」と「パスワード(確認用)」に入力された値が異なっています");
        }
        if (StringUtils.isBlank(name) ) {
        	messages.add("名前を入力してください");
        }
        if (name.length() > 10 ) {
        	messages.add("名前は10文字以下で入力してください");
        }
		List<Branch> branches = new BranchService().getBranch();
        List<Position> positions = new PositionService().getPosition();
        if ( branchId.equals("1") &&
        	 !(positionId.equals("1") || positionId.equals("2")) ) {
        	messages.add(
        		"所属支店で「" +
        		branches.get(0).getName() +
        		"」を選択された場合、「" +
        		positions.get(0).getName() +
        		"」か「" +
        		positions.get(1).getName() +
        		"」を選択してください");
        } else if ( !branchId.equals("1") &&
        		(positionId.equals("1") || positionId.equals("2")) ) {
        	messages.add(
            		"所属支店で「" +
            		branches.get(0).getName() +
            		"」以外を選択された場合、「" +
            		positions.get(0).getName() +
            		"」か「" +
            		positions.get(1).getName() +
            		"」以外を選択してください");
        }
        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }
}