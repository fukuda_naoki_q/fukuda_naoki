package board.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import board.beans.Branch;
import board.beans.Position;
import board.beans.User;
import board.exception.NoRowsUpdatedRuntimeException;
import board.service.BranchService;
import board.service.PositionService;
import board.service.UserService;

@WebServlet(urlPatterns = { "/settings" })
public class SettingsServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
        if ( ((User) session.getAttribute("loginUser")).getBranchId() != 1 ||
       		 ((User) session.getAttribute("loginUser")).getPositionId() != 1) {
        	response.sendRedirect("./");
        	return;
        }
        String editedUserId = null;
        User editUser = null;
        List<String> messages = new ArrayList<String>();
        try {
        	editedUserId = request.getParameter("user_id");
        	editUser = new UserService().getUser(Integer.valueOf(editedUserId));
        	String.valueOf(editUser.getBranchId()).isEmpty();
        } catch (NullPointerException e){
            messages.add("不正なデータが選択されました");
            session.setAttribute("errorMessages", messages);
        	response.sendRedirect("userManager");
            return;
        } catch (NumberFormatException e){
            messages.add("不正なデータが選択されました");
            session.setAttribute("errorMessages", messages);
        	response.sendRedirect("userManager");
            return;
        }
		List<Branch> branches = new BranchService().getBranch();
        List<Position> positions = new PositionService().getPosition();
        request.setAttribute("editUser", editUser);
        request.setAttribute("branches", branches);
        request.setAttribute("positions", positions);
        request.setAttribute("loginUserId", ((User) session.getAttribute("loginUser")).getLoginId());
        request.setAttribute("editedLoginId", editUser.getLoginId());
        request.setAttribute("selectedBranchId", editUser.getBranchId());
        request.setAttribute("selectedPositionId", editUser.getPositionId());
        request.getRequestDispatcher("settings.jsp").forward(request, response);
    }
    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        List<String> messages = new ArrayList<String>();
        HttpSession session = request.getSession();
        User editUser = getEditUser(request);
		List<Branch> branches = new BranchService().getBranch();
        List<Position> positions = new PositionService().getPosition();
        if (isValid(request, editUser, messages) == true) {
            try {
                if (StringUtils.isBlank(request.getParameter("password")) &&
                    StringUtils.isBlank(request.getParameter("confirm")) ) {
                	new UserService().update(editUser, false);
                } else {
                	new UserService().update(editUser, true);
                }
            } catch (NoRowsUpdatedRuntimeException e) {
                messages.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
                session.setAttribute("errorMessages", messages);
                request.setAttribute("editUser", editUser);
                request.setAttribute("branches", branches);
                request.setAttribute("positions", positions);
                request.setAttribute("loginUserId", ((User) session.getAttribute("loginUser")).getLoginId());
                request.setAttribute("editedLoginId", editUser.getLoginId());
                request.setAttribute("selectedBranchId", editUser.getBranchId());
                request.setAttribute("selectedPositionId", editUser.getPositionId());
                request.getRequestDispatcher("settings.jsp").forward(request, response);
                return;
            }
            response.sendRedirect("userManager");
        } else {
            session.setAttribute("errorMessages", messages);
            request.setAttribute("editUser", editUser);
            request.setAttribute("branches", branches);
            request.setAttribute("positions", positions);
            request.setAttribute("loginUserId", ((User) session.getAttribute("loginUser")).getLoginId());
            request.setAttribute("editedLoginId", editUser.getLoginId());
            request.setAttribute("selectedBranchId", editUser.getBranchId());
            request.setAttribute("selectedPositionId", editUser.getPositionId());
            request.getRequestDispatcher("settings.jsp").forward(request, response);
        }
    }
    private User getEditUser(HttpServletRequest request)
            throws IOException, ServletException {
        User editUser = new User();
        editUser.setId(Integer.valueOf(request.getParameter("id")));
        editUser.setLoginId(request.getParameter("loginId"));
        if (StringUtils.isBlank(request.getParameter("password")) == true &&
                StringUtils.isBlank(request.getParameter("confirm")) == true) {
            	User user = new UserService().getUser(Integer.valueOf(request.getParameter("id")));
            	editUser.setPassword(user.getPassword());
        } else {
            editUser.setPassword(request.getParameter("password"));
        }
        editUser.setName(request.getParameter("name"));
		HttpSession session = request.getSession();
        if ( ((User)session.getAttribute("loginUser")).getId() != editUser.getId() ) {
            editUser.setBranchId(Integer.valueOf(request.getParameter("branch_id")));
            editUser.setPositionId(Integer.valueOf(request.getParameter("position_id")));
        } else {
            editUser.setBranchId(((User)session.getAttribute("loginUser")).getBranchId());
            editUser.setPositionId(((User)session.getAttribute("loginUser")).getPositionId());
        }
        return editUser;
    }
    private boolean isValid(HttpServletRequest request, User editUser, List<String> messages) {
        String inputLoginId = request.getParameter("loginId");
        String password = request.getParameter("password");
        String confirm = request.getParameter("confirm");
        String name = request.getParameter("name");
        String branchId = request.getParameter("branch_id");
        String positionId = request.getParameter("position_id");
        String idPattern = "[0-9a-zA-Z]{6,20}";
        String pwPattern = "^[ -~]{6,20}$";
        if (StringUtils.isBlank(inputLoginId)) {
            messages.add("アカウント名を入力してください");
        }
        if (!StringUtils.isBlank(password) &&
        	StringUtils.isBlank(confirm)) {
            messages.add("パスワード(確認用)を入力してください");
        }
        if (StringUtils.isBlank(password) &&
         	!StringUtils.isBlank(confirm) ) {
            messages.add("パスワードを入力してください");
        }
        if (StringUtils.isBlank(name)) {
            messages.add("名前を入力してください");
        }
        if (!inputLoginId.matches(idPattern)) {
            messages.add("ログインIDは半角英数字の6文字以上20文字以下で入力してください");
        }
        if ( !StringUtils.isBlank(password) &&
             !StringUtils.isBlank(confirm) &&
        	 !password.matches(pwPattern)) {
            messages.add("パスワードは記号を含む全ての半角文字6文字以上20文字以下で入力してください");
        }
        if ( !StringUtils.isBlank(password) &&
             !StringUtils.isBlank(confirm) &&
             !password.matches(confirm) ) {
            messages.add("「パスワード」と「パスワード(確認用)」に入力された値が異なっています");
        }
        if (StringUtils.length(name) > 10) {
        	messages.add("名前は10文字以下で入力してください");
        }
        //loginIdの重複チェック
        UserService userService = new UserService();
        User user = userService.getUserByLoginId(inputLoginId);
        if ( user != null && user.getId() != editUser.getId() ) {
        	messages.add("入力されたログインIDは既に使用されています");
        }
		List<Branch> branches = new BranchService().getBranch();
        List<Position> positions = new PositionService().getPosition();
        if ( branchId.equals("1") &&
        	 !(positionId.equals("1") || positionId.equals("2")) ) {
        	messages.add(
        		"所属支店で「" +
        		branches.get(0).getName() +
        		"」を選択された場合、「" +
        		positions.get(0).getName() +
        		"」か「" +
        		positions.get(1).getName() +
        		"」を選択してください");
        } else if ( !branchId.equals("1") &&
        		(positionId.equals("1") || positionId.equals("2")) ) {
        	messages.add(
            		"所属支店で「" +
            		branches.get(0).getName() +
            		"」以外を選択された場合、「" +
            		positions.get(0).getName() +
            		"」か「" +
            		positions.get(1).getName() +
            		"」以外を選択してください");
        }
        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }
}
