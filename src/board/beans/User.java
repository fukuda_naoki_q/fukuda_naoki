package board.beans;

import java.io.Serializable;
import java.util.Date;

public class User implements Serializable {
    private static final long serialVersionUID = 1L;

    private int id;
    private String login_id;
    private String password;
    private String name;
    private int branch_id;
    private String branch_name;
    private int position_id;
    private String position_name;
    private boolean is_stopped;
    private Date created_at;
    private Date updated_at;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getLoginId() {
		return login_id;
	}
	public void setLoginId(String login_id) {
		this.login_id = login_id;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getBranchId() {
		return branch_id;
	}
	public void setBranchId(int branch_id) {
		this.branch_id = branch_id;
	}
	public int getPositionId() {
		return position_id;
	}
	public void setPositionId(int position_id) {
		this.position_id = position_id;
	}
	public boolean getIsStopped() {
		return is_stopped;
	}
	public void setIsStopped(boolean is_stopped) {
		this.is_stopped = is_stopped;
	}
	public Date getCreatedDate() {
		return created_at;
	}
	public void setCreatedDate(Date created_at) {
		this.created_at = created_at;
	}
	public Date getUpdatedDate() {
		return updated_at;
	}
	public void setUpdatedDate(Date updated_at) {
		this.updated_at = updated_at;
	}
	public String getPositionName() {
		return position_name;
	}
	public void setPositionName(String positionName) {
		this.position_name = positionName;
	}
	public String getBranchName() {
		return branch_name;
	}
	public void setBranchName(String branchName) {
		this.branch_name = branchName;
	}
}