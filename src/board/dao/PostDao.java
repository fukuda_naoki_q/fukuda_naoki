package board.dao;

import static board.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import board.beans.Post;
import board.exception.SQLRuntimeException;

public class PostDao {

    public void insert(Connection connection, Post post) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO posts ( ");
            sql.append("title");
            sql.append(", text");
            sql.append(", category");
            sql.append(", user_id");
            sql.append(", created_at");
            sql.append(", updated_at");
            sql.append(") VALUES (");
            sql.append(" ?"); // title
            sql.append(", ?"); // text
            sql.append(", ?"); // category
            sql.append(", ?"); // user_id
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(", CURRENT_TIMESTAMP"); // updated_date
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, post.getTitle());
            ps.setString(2, post.getText());
            ps.setString(3, post.getCategory());
            ps.setString(4, post.getUserId());

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    public List<Post> getUserPosts(Connection connection, int num, String startDate, String endDate, String category) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("posts.id as id, ");
            sql.append("posts.title as title, ");
            sql.append("posts.text as text, ");
            sql.append("posts.category as category, ");
            sql.append("posts.user_id as user_id, ");
            sql.append("posts.created_at as created_at, ");
            sql.append("posts.updated_at as updated_at, ");
            sql.append("users.name ");
            sql.append("FROM posts ");
            sql.append("INNER JOIN users ");
            sql.append("ON posts.user_id = users.id ");
            sql.append("WHERE ");
            sql.append("? <= posts.created_at ");
            sql.append("AND ");
            sql.append("posts.created_at <= ? ");
            if ( startDate.matches("") ) {
            	startDate = "0000-00-00";
            }
            if ( endDate.matches("") ) {
            	endDate = "9999-99-99";
            } else {
            	endDate = endDate + " 23:59:59";
            }
            if ( !category.matches("") ) {
            	sql.append(" AND ");
            	sql.append("posts.category LIKE ? ");
                sql.append("ORDER BY created_at DESC limit " + num);
                ps = connection.prepareStatement(sql.toString());
                ps.setString(1, startDate);
                ps.setString(2, endDate);
                ps.setString(3, "%" + category + "%");
            } else {
                sql.append("ORDER BY created_at DESC limit " + num);
                ps = connection.prepareStatement(sql.toString());
                ps.setString(1, startDate);
                ps.setString(2, endDate);
            }
            ResultSet rs = ps.executeQuery();
            List<Post> ret = toPostList(rs);
            return ret;
    	} catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<Post> toPostList(ResultSet rs)
            throws SQLException {

        List<Post> ret = new ArrayList<Post>();
        try {
            while (rs.next()) {
            	//投稿
                int id = rs.getInt("id");
                String title = rs.getString("title");
                String text = rs.getString("text");
                String category = rs.getString("category");
                String userId = rs.getString("user_id");
                Timestamp createdAt = rs.getTimestamp("created_at");
                Timestamp updatedAt = rs.getTimestamp("updated_at");

                //投稿者名
                String name = rs.getString("name");

                Post post = new Post();
                post.setId(id);
                post.setTitle(title);
                post.setText(text);
                post.setCategory(category);
                post.setUserId(userId);
                post.setName(name);
                post.setCreatedAt(createdAt);
                post.setUpdatedAt(updatedAt);
                ret.add(post);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

    public void delete(Connection connection, Post post) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("DELETE FROM posts ");
            sql.append("WHERE ");
            sql.append("id = ?");

            ps = connection.prepareStatement(sql.toString());

            ps.setInt(1, post.getId());

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
}