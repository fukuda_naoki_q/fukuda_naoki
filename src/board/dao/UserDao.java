package board.dao;

import static board.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import board.beans.User;
import board.exception.NoRowsUpdatedRuntimeException;
import board.exception.SQLRuntimeException;

public class UserDao {

    public void insert(Connection connection, User user) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO users ( ");
            sql.append("login_id");
            sql.append(", password");
            sql.append(", name");
            sql.append(", branch_id");
            sql.append(", position_id");
            sql.append(", is_stopped");
            sql.append(", created_at");
            sql.append(", updated_at");
            sql.append(") VALUES (");
            sql.append("?"); // login_id
            sql.append(", ?"); // password
            sql.append(", ?"); // name
            sql.append(", ?"); // branch_id
            sql.append(", ?"); // position_id
            sql.append(", ?"); // is_stopped
            sql.append(", CURRENT_TIMESTAMP"); // created_at
            sql.append(", CURRENT_TIMESTAMP"); // updated_at
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, user.getLoginId());
            ps.setString(2, user.getPassword());
            ps.setString(3, user.getName());
            ps.setInt(4, user.getBranchId());
            ps.setInt(5, user.getPositionId());
            ps.setBoolean(6, user.getIsStopped());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    public User getUser(Connection connection, String loginId,
            String password) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM users WHERE login_id = ? AND password = ?";

            ps = connection.prepareStatement(sql);
            ps.setString(1, loginId);
            ps.setString(2, password);

            ResultSet rs = ps.executeQuery();
            List<User> userList = toUserList(rs);
            if (userList.isEmpty() == true) {
                return null;
            } else if (2 <= userList.size()) {
                throw new IllegalStateException("2 <= userList.size()");
            } else {
                return userList.get(0);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<User> toUserList(ResultSet rs) throws SQLException {

        List<User> ret = new ArrayList<User>();
        try {
            while (rs.next()) {
                int id = rs.getInt("id");
                String loginId = rs.getString("login_id");
                String password = rs.getString("password");
                String name = rs.getString("name");
                int branchId = rs.getInt("branch_id");
                int positionId = rs.getInt("position_id");
                boolean isStopped = rs.getBoolean("is_stopped");
                Timestamp createdDate = rs.getTimestamp("created_at");
                Timestamp updatedDate = rs.getTimestamp("updated_at");

                User user = new User();
                user.setId(id);
                user.setLoginId(loginId);
                user.setPassword(password);
                user.setName(name);
                user.setBranchId(branchId);
                user.setPositionId(positionId);
                user.setIsStopped(isStopped);
                user.setCreatedDate(createdDate);
                user.setUpdatedDate(updatedDate);
                ret.add(user);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

    private List<User> toUserListWithBranchesAndPositionsName(ResultSet rs) throws SQLException {

        List<User> ret = new ArrayList<User>();
        try {
            while (rs.next()) {
                int id = rs.getInt("id");
                String loginId = rs.getString("login_id");
                String password = rs.getString("password");
                String name = rs.getString("name");
                int branchId = rs.getInt("branch_id");
                int positionId = rs.getInt("position_id");
                boolean isStopped = rs.getBoolean("is_stopped");
                Timestamp createdDate = rs.getTimestamp("created_at");
                Timestamp updatedDate = rs.getTimestamp("updated_at");
                String branchName = rs.getString("branches.name");
                String positionName = rs.getString("positions.name");

                User user = new User();
                user.setId(id);
                user.setLoginId(loginId);
                user.setPassword(password);
                user.setName(name);
                user.setBranchId(branchId);
                user.setPositionId(positionId);
                user.setIsStopped(isStopped);
                user.setCreatedDate(createdDate);
                user.setUpdatedDate(updatedDate);
                user.setBranchName(branchName);
                user.setPositionName(positionName);
                ret.add(user);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

    public User getUser(Connection connection, int id) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM users WHERE id = ?";

            ps = connection.prepareStatement(sql);
            ps.setInt(1, id);

            ResultSet rs = ps.executeQuery();
            List<User> userList = toUserList(rs);
            if (userList.isEmpty() == true) {
                return null;
            } else if (2 <= userList.size()) {
                throw new IllegalStateException("2 <= userList.size()");
            } else {
                return userList.get(0);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    public List<User> getUsers(Connection connection, int num) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("users.id as id, ");
            sql.append("users.login_id as login_id, ");
            sql.append("users.password as password, ");
            sql.append("users.name as name, ");
            sql.append("users.branch_id as branch_id, ");
            sql.append("users.position_id as position_id, ");
            sql.append("users.is_stopped as is_stopped, ");
            sql.append("users.created_at as created_at, ");
            sql.append("users.updated_at as updated_at ");
            sql.append("FROM users ");
            sql.append("ORDER BY id ASC limit " + num);

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<User> ret = toUserList(rs);
            return ret;
        	} catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

	public void update(Connection connection, User user) {
        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE users SET");
            sql.append("  login_id = ?");
            sql.append(", password = ?");
            sql.append(", name = ?");
            sql.append(", branch_id = ?");
            sql.append(", position_id = ?");
            sql.append(", is_stopped = ?");
            sql.append(", updated_at = CURRENT_TIMESTAMP");
            sql.append(" WHERE");
            sql.append(" id = ?");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, user.getLoginId());
            ps.setString(2, user.getPassword());
            ps.setString(3, user.getName());
            ps.setInt(4, user.getBranchId());
            ps.setInt(5, user.getPositionId());
            ps.setBoolean(6, user.getIsStopped());
            ps.setInt(7, user.getId());

            int count = ps.executeUpdate();
            if (count == 0) {
                throw new NoRowsUpdatedRuntimeException();
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

	public User getUserByLoginId(Connection connection, String loginId) {
        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM users WHERE login_id = ?";
            ps = connection.prepareStatement(sql);
            ps.setString(1, loginId);
            ResultSet rs = ps.executeQuery();
            List<User> userList = toUserList(rs);
            if (userList.isEmpty() == true) {
                return null;
            } else if (2 <= userList.size()) {
                throw new IllegalStateException("2 <= userList.size()");
            } else {
                return userList.get(0);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    public List<User> getUsersWithBranchesNameAndPositionsName(Connection connection, int num) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("users.id as id, ");
            sql.append("users.login_id as login_id, ");
            sql.append("users.password as password, ");
            sql.append("users.name as name, ");
            sql.append("users.branch_id as branch_id, ");
            sql.append("users.position_id as position_id, ");
            sql.append("users.is_stopped as is_stopped, ");
            sql.append("users.created_at as created_at, ");
            sql.append("users.updated_at as updated_at, ");
            sql.append("branches.name, ");
            sql.append("positions.name ");
            sql.append("FROM ( users ");
            sql.append("INNER JOIN branches ");
            sql.append("ON users.branch_id = branches.id ) ");
            sql.append("INNER JOIN positions ");
            sql.append("ON users.position_id = positions.id  ");
            sql.append("ORDER BY id ASC limit " + num);

            ps = connection.prepareStatement(sql.toString());


            ResultSet rs = ps.executeQuery();
            List<User> ret = toUserListWithBranchesAndPositionsName(rs);
            return ret;
        	} catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
}