package board.dao;

import static board.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import board.beans.Branch;
import board.exception.SQLRuntimeException;

public class BranchDao {
    public Branch getBranch(Connection connection, int branch_id) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM branches WHERE id = ??";

            ps = connection.prepareStatement(sql);
            ps.setInt(1, branch_id);

            ResultSet rs = ps.executeQuery();
            List<Branch> branchList = toBranchList(rs);
            if (branchList.isEmpty() == true) {
                return null;
            } else if (2 <= branchList.size()) {
                throw new IllegalStateException("2 <= userList.size()");
            } else {
                return branchList.get(0);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    public List<Branch> getBranches(Connection connection, int num) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("branches.id as id, ");
            sql.append("branches.name as name, ");
            sql.append("branches.created_at as created_at ");
            sql.append("FROM branches ");

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<Branch> ret = toBranchList(rs);
            return ret;
        	} catch (SQLException e) {
        		throw new SQLRuntimeException(e);
        	} finally {
        		close(ps);
    	}
    }

    private List<Branch> toBranchList(ResultSet rs)
            throws SQLException {

        List<Branch> ret = new ArrayList<Branch>();
        try {
            while (rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");
                Timestamp createdAt = rs.getTimestamp("created_at");

                Branch branch = new Branch();
                branch.setId(id);
                branch.setName(name);
                branch.setCreatedAt(createdAt);

                ret.add(branch);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

}