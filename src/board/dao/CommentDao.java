package board.dao;

import static board.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import board.beans.Comment;
import board.exception.SQLRuntimeException;

public class CommentDao {

    public void insert(Connection connection, Comment comment) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO comments ( ");
            sql.append("post_id");
            sql.append(", text");
            sql.append(", user_id");
            sql.append(", created_at");
            sql.append(", updated_at");
            sql.append(") VALUES (");
            sql.append(" ?"); // post_id
            sql.append(", ?"); // text
            sql.append(", ?"); // user_id
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(", CURRENT_TIMESTAMP"); // updated_date
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, comment.getPostId());
            ps.setString(2, comment.getText());
            ps.setString(3, comment.getUserId());

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    public List<Comment> getUserComments(Connection connection, int num) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("comments.id as id, ");
            sql.append("comments.post_id as post_id, ");
            sql.append("comments.text as text, ");
            sql.append("comments.user_id as user_id, ");
            sql.append("comments.created_at as created_at, ");
            sql.append("comments.updated_at as updated_at, ");
            sql.append("posts.id, ");
            sql.append("users.name ");
            sql.append("FROM comments ");
            sql.append("INNER JOIN users ");
            sql.append("ON comments.user_id = users.id ");
            sql.append("INNER JOIN posts ");
            sql.append("ON comments.post_id = posts.id ");
            sql.append("ORDER BY created_at ASC limit " + num);

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<Comment> ret = toCommentList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<Comment> toCommentList(ResultSet rs)
            throws SQLException {

        List<Comment> ret = new ArrayList<Comment>();
        try {
            while (rs.next()) {
                int id = rs.getInt("id");
                String postId = rs.getString("post_id");
                String text = rs.getString("text");
                String userId = rs.getString("user_id");
                String username = rs.getString("users.name");
                Timestamp createdAt = rs.getTimestamp("created_at");
                Timestamp updatedAt = rs.getTimestamp("updated_at");

                Comment comment = new Comment();
                comment.setId(id);
                comment.setPostId(postId);
                comment.setText(text);
                comment.setUserId(userId);
                comment.setUsername(username);
                comment.setCreatedAt(createdAt);
                comment.setUpdatedAt(updatedAt);

                ret.add(comment);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

    public void delete(Connection connection, Comment comment) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("DELETE FROM comments ");
            sql.append("WHERE ");
            sql.append("id = ?");

            ps = connection.prepareStatement(sql.toString());

            ps.setInt(1, comment.getId());

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

}