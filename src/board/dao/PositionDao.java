package board.dao;

import static board.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import board.beans.Position;
import board.exception.SQLRuntimeException;

public class PositionDao {
    public Position getPosition(Connection connection, int position_id) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM positions WHERE id = ??";

            ps = connection.prepareStatement(sql);
            ps.setInt(1, position_id);

            ResultSet rs = ps.executeQuery();
            List<Position> positionList = toPositionList(rs);
            if (positionList.isEmpty() == true) {
                return null;
            } else if (2 <= positionList.size()) {
                throw new IllegalStateException("2 <= userList.size()");
            } else {
                return positionList.get(0);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    public List<Position> getPositions(Connection connection, int num) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("positions.id as id, ");
            sql.append("positions.name as name, ");
            sql.append("positions.created_at as created_at ");
            sql.append("FROM positions ");

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<Position> ret = toPositionList(rs);
            return ret;
    	} catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<Position> toPositionList(ResultSet rs)
            throws SQLException {

        List<Position> ret = new ArrayList<Position>();
        try {
            while (rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");
                Timestamp createdAt = rs.getTimestamp("created_at");

                Position position = new Position();
                position.setId(id);
                position.setName(name);
                position.setCreatedAt(createdAt);

                ret.add(position);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

}