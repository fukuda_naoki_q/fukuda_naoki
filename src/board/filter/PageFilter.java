package board.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import board.beans.Branch;
import board.beans.Position;
import board.beans.User;
import board.service.BranchService;
import board.service.PositionService;

@WebFilter("/*")
public class PageFilter extends HttpServlet implements Filter {
	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		HttpServletRequest httpRequest = (HttpServletRequest)request;
        HttpSession session = httpRequest.getSession();
        if(httpRequest.getRequestURI().endsWith(".css")){
          chain.doFilter(request,response);
          return;
        }
        String servletPath = httpRequest.getServletPath();
        if ( !servletPath.matches("/login") &&
        	session.getAttribute("loginUser") == null ) {
    		session.setAttribute("errorMessages", "ログインしてください");
            ((HttpServletResponse)response).sendRedirect("login");
            return;
        }
        if ( session.getAttribute("loginUser") != null ) {
        	if ( servletPath.matches("/signup") ||
	    		 servletPath.matches("/settings") ||
	    		 servletPath.matches("/userManager") ) {
	        	List<Branch> branches = new BranchService().getBranch();
	        	List<Position> positions = new PositionService().getPosition();
	        	User user = (User)session.getAttribute("loginUser");
	        	if ( user != null ) {
	        		for ( int i = 0; i < branches.size(); i++ ) {
		        		if ( user != null &&
		        			 1 == user.getBranchId() ) {
		            		for( int j = 0; j < positions.size(); j++ ) {
		            			if ( 1 == user.getPositionId() ) {
		            	        	chain.doFilter(request, response); // サーブレットを実行
		            	        	return;
		        				}
		        			}
		    			}
		    		}
	        	}
	            List<String> errorComments = new ArrayList<String>();
	            errorComments.add("開こうとしたページは特定の権限を持つユーザーのみ閲覧可能です");
	            session.setAttribute("errorComments", errorComments);
	        	chain.doFilter(request, response); // サーブレットを実行
	        	return;
	        } else {
	        	chain.doFilter(request, response); // サーブレットを実行
	        	return;
	        }
		} else {
			chain.doFilter(request, response); // サーブレットを実行
			return;
		}
	}

	@Override
	public void init(FilterConfig config) {
	}

	@Override
	public void destroy() {
	}

}
